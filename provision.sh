#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# Use Australian closest mirror
sudo sed -i 's,archive.ubuntu.com,mirror.waia.asn.au,g' /etc/apt/sources.list

sudo apt-get update
sudo apt-get --yes upgrade
sudo apt-get --yes clean autoremove

sudo apt-add-repository --yes ppa:ansible/ansible
sudo apt-get update

sudo apt-get -qq install -y make git ansible=$(apt-cache madison ansible | cut -d '|' -f2 | grep ppa | tr -d '[:space:]')