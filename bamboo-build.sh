#!/usr/bin/env bash

# chmod +x gradlew
# ./gradlew clean build

docker pull gradle:jdk8
docker run --user root --rm -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:jdk8 gradle clean build
sudo chown bamboo:bamboo -R ./ # ensures the 