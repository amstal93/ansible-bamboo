# Ansible / Bamboo

Setup a Bamboo server using Ansible

## Installation

Install the requirements first:

```bash
make install
```

## Converge

Converge the Ansible playbook into the desired state

```bash
make converge
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)