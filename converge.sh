#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo ansible-galaxy install -r requirements.yml
sudo ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i 'localhost,' install.yml